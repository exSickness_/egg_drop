CFLAGS+=-std=c11 -Wall -Werror -Wno-deprecated -Wextra -Wstack-usage=1024 -pedantic -fstack-usage -D _XOPEN_SOURCE=800


egg_drop: egg_drop.o egg.o -lm


.PHONY: clean debug profile

clean:
	rm egg_drop *.o *.su

debug: CFLAGS+=-g
debug: egg_drop

profile: CFLAGS+=-pg
profile: LDFLAGS+=-pg
profile: egg_drop