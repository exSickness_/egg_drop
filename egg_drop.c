#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "egg.h"
#include "egg_drop.h"

// Do not enter floor number below the safe floor. Undefined behavior.

int main(int argc, char* argv[])
{
	int eggs = 0;
	size_t floors = 0;
	if(argc == 1)
	{
		eggs = 2;
		floors = 100;
	}
	else if(argc == 3) {
		char* endptr;
		floors = strtol(argv[1],&endptr,10);
		if(endptr == argv[1] || floors <= 0 || floors > 16000) {
			fprintf(stderr,"Invalid floor number.\n");
			return(-1);
		}
		eggs = strtol(argv[2],&endptr,10);
		if(endptr == argv[2] || eggs <= 0 || eggs > 16000) {
			fprintf(stderr,"Invalid egg number.\n");
			return(-1);
		}
	}
	else
	{
		fprintf(stderr,"Invalid number of arguments.\n");
		return(-1);
	}

	printf("Floors:%zu and Eggs:%d\n\n",floors,eggs);

	egg** carton = fillCarton(eggs);
	if(carton == NULL) {
		fprintf(stderr,"Error laying egg.\n");
		return(-1);
	}

	int dropCount = 0;
	int safeFloor = 0;
	safeFloor = quadraticSolution(carton,floors,eggs,&dropCount);
	printf("\n\t%d is the max safe floor. Found after %d drops.\n\n",safeFloor,dropCount);

	omletteTime(carton,eggs);
	return(0);
}


int quadraticSolution(egg** carton, size_t floors,int eggNum, int* count)
{
	size_t floorToDrop=0;
	size_t high = floors;
	size_t low = 0;
	double baseNum = 0;

	baseNum = (((-1 * 0.5) + sqrt( ( (0.5*0.5) - ( 4*0.5*(((double)floors)* -1) )  ))) / (2*0.5));
	baseNum++;
	floorToDrop = baseNum;

	size_t step = 0;
	int i = 0;
	// loop for all except one egg
	for( ; i < eggNum -1; i++)
	{
		// first egg loop. finds low and high
		while(!egg_is_broken(carton[i]))
		{
			egg_drop_from_floor(carton[i],floorToDrop);
			printf("Dropping egg from %zu: %s\n",floorToDrop,egg_is_broken(carton[i]) ? "CRACK" : "Safe");
			*count+=1;

			 // EGG BROKE
			if(egg_is_broken(carton[i])) {
				high = floorToDrop;
				baseNum = (((-1 * 0.5) + sqrt( ( (0.5*0.5) - ( 4*0.5*(((double)high-low)* -1) )  ))) / (2*0.5));

				floorToDrop = low +1;
			}
			// EGG DIDN'T BREAK
			else {
				low = floorToDrop;
				step = low + baseNum;

				floorToDrop = step;
				if(floorToDrop == high) {
					floorToDrop--;
				}
			}
			// Check for one thing between low and high
			if(high - low < 2) {
				return(high-1);
			}
			// the algorithm overshot the total number of floors. recalcuating
			if(floorToDrop >= floors) {
				floorToDrop -= baseNum;
				baseNum = (((-1 * 0.5) + sqrt( ( (0.5*0.5) - ( 4*0.5*(((double)high-low)* -1) )  ))) / (2*0.5));
				floorToDrop += baseNum;
			}
		}
	}

	floorToDrop = low +1;
	// loop for one egg starting at low+1
	while(!egg_is_broken(carton[i]))
	{
		// check for special case
		if(floorToDrop == 1) {
			egg_drop_from_floor(carton[i],floorToDrop);
			if(egg_is_broken(carton[i])) {
				printf("Dropping egg from 1: CRACK\n");
				return(-2);
			}
			printf("Dropping egg from 1: Safe\n");
		}
		else
		{
			egg_drop_from_floor(carton[i],floorToDrop);
			printf("Dropping egg from %zu: %s\n",floorToDrop,egg_is_broken(carton[i]) ? "CRACK" : "Safe");
		}

		*count+= 1;
		floorToDrop++;
		if(floorToDrop >= floors) {
			return(floors);
		}
	}
	return(floorToDrop-2);
}


egg** fillCarton(int eggs)
{
	egg** carton = malloc(sizeof(egg*) * eggs);
	for(int i = 0;i < eggs; i++)
	{
		carton[i] = lay_egg();
		if(carton[i] == NULL) {
			free(carton);
			return(NULL);
		}
	}

	return(carton);
}

void omletteTime(egg** carton,int eggNum )
{
	for(int i = eggNum-1; i >= 0; i--)
	{
		cook_egg(carton[i]);
	}
	free(carton);
}
