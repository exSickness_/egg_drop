#ifndef SCRAMBLED_H
	#define SCRAMBLED_H

int quadraticSolution(egg** carton, size_t floors,int eggNum, int* dropCount);
void omletteTime(egg** carton,int eggNum );
egg** fillCarton(int eggs);

#endif