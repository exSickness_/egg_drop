
#include "egg.h"

static const int SECRET_FLOOR_LIMIT = 7;

egg* lay_egg(void)
{
	egg* e = malloc( sizeof(*e) );
	if(e) {
		e->id = SECRET_FLOOR_LIMIT;
	}

	return(e);
}

void cook_egg(egg* e)
{
	free(e);
}

bool egg_is_broken(egg* e)
{
	return(e->id == -1);
}

void egg_drop_from_floor(egg* e, size_t floor)
{
	if(e && (size_t)e->id < floor ) {
		e->id = -1;
	}
}